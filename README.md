# File-online-preview

H5 | Supports online preview of files in pdf, epub, and djvu formats.

## Library

- [pdf.js](https://github.com/mozilla/pdf.js)
- [epub.js](https://github.com/futurepress/epub.js)
- [djvu.js](https://github.com/RussCoder/djvujs)

pdf.js: [LICENSE](./pdf.js/LICENSE)
